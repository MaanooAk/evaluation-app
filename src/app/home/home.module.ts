import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { IndexComponent } from './index/index.component';
import { RouterModule } from '@angular/router';
import { EvaluateComponent } from './evaluate/evaluate.component';
import { FormioModule } from '@formio/angular';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [
    IndexComponent,
    EvaluateComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    TranslateModule,
    HomeRoutingModule,
    FormioModule,
    NgxMaskModule.forChild()
  ]
})
export class HomeModule { }

import {ApplicationSettingsConfiguration, SharedModule} from '@universis/common';

export declare interface ApplicationSettings extends ApplicationSettingsConfiguration {
  title?: string;
  header?: Array<any>;
  image?: string;
}
